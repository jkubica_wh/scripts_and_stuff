#PROD
stacks=( sad21 sad22 sad23 sad24 sad25 sad26 sai01 sai02 sai03 sai04 sai05 sai06 sae01 )
cell=prd

#PP1
#stacks=( sad01 sad02 sad03 sad04 sad05 sad06 sai01 sai02 sai03 sai04 sai05 sai06 sae01 )
#cell=pp1

dirname=CFGDUMP
mkdir ./$dirname

echo "" > hosts_to_call_yo

for i in "${stacks[@]}"
do
  :
	echo $i
  cx container location --stack-tag $i --cell $cell --json | jq '.response | to_entries[].value.task' -r >> hosts_to_call_yo
done


cat hosts_to_call_yo | while read line 
do
  CFG_NAME=$(curl -s $line/healthcheck/env | jq .env.WHC_ROLE -r)
  CFG_NAME_EXTRA=$(curl -s $line/healthcheck/env | jq .env.MARATHON_APP_LABEL_COM_WHC_SLAVE_CLUSTER -r)
  echo $CFG_NAME
  echo $CFG_NAME_EXTRA
  curl -s $line/healthcheck/config > ./$dirname/$CFG_NAME_EXTRA-$CFG_NAME.json
done 
