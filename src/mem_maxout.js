const MB = 1024 * 1024;

const arrayOfBlocks = [];

function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};


process.on('SIGTERM', () => {
    console.log(process.memoryUsage());
    process.exit();
});


for (i=0; true; i++) {
	
	let currentBuffer = new Uint8Array(MB);
	
	// fill with gargabage data, this is needed due to 
  // memory management (google: linux lazy memory allocation)
  // random is used to generate high entropy to avoid zram compression 
	
  for (j=0;j<MB;j+=4) {
    let rnd = Math.random() * 64 // random is slow
		currentBuffer[j] = rnd;
    currentBuffer[j+1] = rnd*2;
    currentBuffer[j+2] = rnd*3;
    currentBuffer[j+3] = rnd*4;
  }
	
	arrayOfBlocks.push(currentBuffer);
	
	if (i%102===0) {
		console.log(`alloc ${i} MB    RSS=` , bytesToSize(process.memoryUsage.rss()) );
		
	}
};
